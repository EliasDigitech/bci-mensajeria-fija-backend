# rest_framework
from rest_framework import serializers

# models
from ..models import Companies, Branches, User

class CompaniesReportSerializer(serializers.ModelSerializer):
    company_type = serializers.SerializerMethodField()
    TYPES = dict(
        client="Cliente",
        owner="Propietario"
    )
    class Meta:
        model = Companies
        fields = (
            'rut',
            'name',
            'commercial_business',
            'company_type',
        )

    def get_company_type(self, instance):
        return self.TYPES.get(instance.company_type, instance.company_type)

class BranchesReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Branches
        fields = (
            'name',
            'address',
            'comune',
            'region',
            'contact_name',
            'phone',
            'email',
        )

class UsersReportSerializer(serializers.ModelSerializer):
    branch = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = (
            'rut',
            'first_name',
            'last_name',
            'username',
            'phone',
            'email',
            'branch'
        )
    
    def get_branch(self, instance):
        return instance.branch.name