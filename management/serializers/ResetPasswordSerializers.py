from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from ..models import User
from utils.FieldsValidators import token_validator

class ResetPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate_email(self, value):
        if not User.objects.filter(email=value).exists():
            raise serializers.ValidationError(detail=['No existe usuario con el email indicado.'])
        else:
            return value


class ConfirmPassword(serializers.Serializer):
    password = serializers.CharField(
        required=True,
        allow_blank=False,
        trim_whitespace=True,
        min_length=8,
        max_length=50,
    )
    confirm_password = serializers.CharField(
        required=True,
        allow_blank=False,
        trim_whitespace=True,
        min_length=8,
        max_length=50,
    )


class ResetPasswordCompleteSerializer(ConfirmPassword):
    token = serializers.CharField(
        required=True,
        allow_blank=False
    )

    def validate(self, data):
        if data['password'] != data['confirm_password']:
            raise serializers.ValidationError(detail=['Las contraseñas no coinciden'])
        else:
            token = token_validator(data['token'], 'reset_password')
            data['token_decode'] = token
            return data