# django
from django.conf import settings
# from django.contrib.auth import get_user_model

# rest framework
from rest_framework_simplejwt.tokens import UntypedToken
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError

# etc
from jwt import decode as jwt_decode
from urllib.parse import parse_qs
# from channels.db import close_old_connections, database_sync_to_async

class SocketJWTMiddleware:

    def __init__(self, inner):
        self.inner = inner

    # def get_user(self, decoded_data):
    #     return get_user_model().objects.get(id=decoded_data["user_id"])

    def __call__(self, scope):
        # close_old_connections()
        token = parse_qs(scope["query_string"].decode("utf8"))["token"][0]
        try:
            UntypedToken(token)
        except (InvalidToken, TokenError) as e:
            user_id = None
        else:
            decoded_data = jwt_decode(token, settings.SECRET_KEY, algorithms=["HS512"])
            user_id = decoded_data.get('user_id', None)
        return self.inner(dict(scope, user_id=user_id))