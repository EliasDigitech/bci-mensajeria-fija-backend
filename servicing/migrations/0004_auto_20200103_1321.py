# Generated by Django 3.0 on 2020-01-03 16:21

from django.conf import settings
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('servicing', '0003_auto_20191218_1322'),
    ]

    operations = [
        migrations.CreateModel(
            name='CompanyFiles',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('company_name', models.CharField(max_length=100)),
                ('rut', models.CharField(max_length=15)),
                ('address', models.CharField(max_length=150)),
                ('phone', models.CharField(blank=True, default=None, max_length=100, null=True)),
                ('contact_name', models.CharField(max_length=150)),
                ('comune', models.CharField(max_length=150)),
                ('province', models.CharField(max_length=150)),
                ('region', models.CharField(max_length=150)),
                ('billing_contacts', django.contrib.postgres.fields.jsonb.JSONField(null=True)),
                ('file_pricings', django.contrib.postgres.fields.jsonb.JSONField(null=True)),
                ('status', models.CharField(choices=[('draft', 'Borrador'), ('sent', 'Enviada'), ('rejected', 'Rechazada'), ('accepted', 'Aprobada')], max_length=20)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'company_files',
            },
        ),
        migrations.AlterField(
            model_name='pricings',
            name='is_default',
            field=models.CharField(choices=[('yes', 'Si'), ('no', 'No')], default='no', max_length=20),
        ),
        migrations.CreateModel(
            name='CompanyFilesLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('stamp', models.DateTimeField(auto_now_add=True)),
                ('status', models.CharField(choices=[('draft', 'Borrador'), ('sent', 'Enviada'), ('rejected', 'Rechazada'), ('accepted', 'Aprobada')], max_length=20)),
                ('company_file', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='servicing.CompanyFiles')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'company_files_log',
            },
        ),
    ]
