from rest_framework import permissions

class CanCreateApproveFile(permissions.BasePermission):
    TRADER_PERMS = ('GET', 'HEAD', 'OPTIONS', 'PUT', 'POST')
    ADMIN_PERMS = ('GET', 'HEAD', 'OPTIONS', 'POST', 'PATCH')

    def has_permission(self, request, view):
        if request.user.is_staff:
            return request.method in self.ADMIN_PERMS
        if request.user.groups.filter(name__in=('traders', 'traders_supervisor')).exists():
            return request.method in self.TRADER_PERMS

class CanModifyReasons(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_staff or request.user.groups.filter(name__in=('traders', 'traders_supervisor')).exists():
            return True
