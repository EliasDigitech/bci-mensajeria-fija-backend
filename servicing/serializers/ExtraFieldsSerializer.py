# rest framework
from rest_framework import serializers

# models
from management.models import Companies

class ExtraFieldsSerializer(serializers.Serializer):
    types_choices = (
        ("text", "Texto"),
        ("number", "Número"),
        ("date", "Fecha"),
    )
    name = serializers.CharField(max_length=20)
    type = serializers.ChoiceField(choices=types_choices)
