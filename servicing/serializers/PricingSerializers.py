from rest_framework import serializers
from django.db.models import QuerySet
from django.db import transaction
from ..models import Services, Segments, Pricings, Products

# REVISAR FUNCIONAMIENTO DE ESTO
from management.models import Companies

class PricingListSerializer(serializers.ListSerializer):

    def update(self, instance, validated_data):
        pricings_mapping = {price.id: price for price in instance}
        data_mapping = {item['id']: item for item in validated_data}
        ret = []
        for price_id, data in data_mapping.items():
            price = pricings_mapping.get(price_id, None)
            if price is None:
                ret.append(self.child.create(data))
            else:
                ret.append(self.child.update(price, data))

        for price_id, price in pricings_mapping.items():
            if price_id not in pricings_mapping:
                price.delete()

        return ret

    def to_representation(self, instance):
        # REVISAR FUNCIONAMIENTO DE ESTO
        if isinstance(instance, QuerySet):
            ret = super().to_representation(instance)
            return ret
        if isinstance(instance.instance, Companies):
            company = instance.instance
        else:
            company = instance.instance.company
        ret = super().to_representation(instance.filter(company=company))
        return ret


class PricingSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    serializers.CharField()
    class Meta:
        model = Pricings
        fields = '__all__'
        list_serializer_class = PricingListSerializer
        extra_kwargs = {
            'service' : {'allow_null': True},
            'segment' : {'allow_null': True}
        }

    def update(self, instance, validated_data):
        return super().update(instance, validated_data)


class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = '__all__'

class SegmentsSerializer(serializers.ModelSerializer):
    segment_price = PricingSerializer(many=True)

    class Meta:
        model = Segments
        fields = '__all__'

    def update(self, instance, validated_data):
        pricings = validated_data.pop('segment_price')
        for el in pricings:
            el_id = el.get('id', None)
            if el_id:
                price = Pricings.objects.get(pk=el_id)
                price.no_return = el.get('no_return')
                price.with_return = el.get('with_return')
                price.save()
        return super().update(instance, validated_data)

    @transaction.atomic
    def create(self, validated_data):
        pricings = validated_data.pop('segment_price')
        ret = super().create(validated_data)
        for el in pricings:
            el['segment'] = ret
            Pricings.objects.create(**el)
        return ret


class ServicesSerializer(serializers.ModelSerializer):
    service_price = PricingSerializer(many=True)

    class Meta:
        model = Services
        fields = '__all__'

    def update(self, instance, validated_data):
        pricings = validated_data.pop('service_price')
        for el in pricings:
            el_id = el.get('id', None)
            if el_id:
                price = Pricings.objects.get(pk=el_id)
                price.no_return = el.get('no_return')
                price.with_return = el.get('with_return')
                price.save()
        return super().update(instance, validated_data)

    @transaction.atomic
    def create(self, validated_data):
        pricings = validated_data.pop('service_price')
        ret = super().create(validated_data)
        for el in pricings:
            el['service'] = ret
            Pricings.objects.create(**el)
        return ret
