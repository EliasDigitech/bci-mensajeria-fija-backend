from django.db import transaction
from rest_framework import serializers

from ..models import FailedReasons, FailedReasonsCompany
from management.serializers.CompaniesSerializers import CompanyOperatorSerializer

class ReasonCompanyRelSerializer(serializers.ModelSerializer):

    class Meta:
        model = FailedReasonsCompany
        fields = '__all__'
        extra_kwargs = {
            'failed_reason': {'required': False}
        }

class ReasonAdmSerializer(serializers.ModelSerializer):
    default_display = serializers.SerializerMethodField()
    failed_reason_rel_reason = ReasonCompanyRelSerializer(many=True, allow_null=True, required=False)

    class Meta:
        model = FailedReasons
        fields = '__all__'

    def get_default_display(self, instance):
        return instance.get_default_display()

    @transaction.atomic
    def update(self, instance, validated_data):
        try:
            reason_map = {rel.company.id: rel for rel in instance.failed_reason_rel_reason.all()}
        except FailedReasonsCompany.DoesNotExist:
            reason_map = {}
        data_map = {item['company'].id: item['company'] for item in validated_data.get('failed_reason_rel_reason', [])}

        for company_id, company in data_map.items():
            rel = reason_map.get(company_id, None)
            if rel is None:
                FailedReasonsCompany.objects.create(company=company, failed_reason=instance)
        
        for company_id, rel in reason_map.items():
            if company_id not in data_map:
                rel.delete()

        if (validated_data.get('failed_reason_rel_reason', None)):
            validated_data.pop('failed_reason_rel_reason')
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['failed_reason_rel_reason'] = [el['company'] for el in ret['failed_reason_rel_reason']]
        return ret