# rest framework
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.settings import api_settings

# models
from ..models import FailedReasons

# serializers
from ..serializers.ReasonsSerializers import ReasonAdmSerializer

#etc
from utils.Permissions import ReadOnly
from ..permissions import CanModifyReasons
from ..filters import ReasonsFilters

class ReasonsViewSet(ModelViewSet):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, CanModifyReasons|ReadOnly, )
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = ReasonsFilters
    serializer_class = ReasonAdmSerializer
    pagination_class = None
    search_fields = (
        'default',
        'reason',
    )

    def get_queryset(self, *args, **kwargs):
        if self.request.user.is_staff or self.request.user.groups.filter(name__in=('traders_supervisor')).exists():
            return FailedReasons.objects.all().order_by('id')
        else:
            return FailedReasons.objects.filter(default='yes').order_by('id')
