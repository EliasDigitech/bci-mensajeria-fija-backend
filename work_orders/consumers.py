#django
from django.contrib.auth import get_user_model
from django.db.models import Sum, Case, When, Value, IntegerField, Q

# channels
from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncJsonWebsocketConsumer

#etc
import json

# serializers
from .serializers.WorkOrdersSocketSerializer import WorkOrdersSocketSerializer

# models
from management.models import Companies
from work_orders.models import WorkOrders

#filter
from .filters.workOrders import CompanyFilter

@database_sync_to_async
def get_user(user_id):
    return get_user_model().objects.get(id=user_id)

@database_sync_to_async
def user_belong(user, group_name):
    return user.groups.filter(name=group_name).exists()

class WONotifier(AsyncJsonWebsocketConsumer):

    @database_sync_to_async
    def get_client_pending(self, filters={}):
        raw_wos = CompanyFilter(data=filters, queryset=Companies.objects.all())
        wos = raw_wos.qs.annotate(
            works_orders=Sum(
                Case(
                    When(
                        Q(company_branches__branches_users__work_orders_owner__extra_fields__print=False) &
                        ~Q(company_branches__branches_users__work_orders_owner__current_step__status__code__in=filters.get('excluded_status__in').split(',')),
                        then=1
                        ),
                    default=Value(0),
                    output_field=IntegerField()
                    )
                )
            ).order_by('-works_orders', 'commercial_business').values('id', 'commercial_business', 'works_orders')
        return WorkOrdersSocketSerializer(wos, many=True).data

    async def connect(self):
        if self.scope['user_id'] is None:
            await self.close()
        user = await get_user(self.scope['user_id'])
        if user.status == 'disabled':
            await self.close()
        if await user_belong(user, 'operators'):
            await self.channel_layer.group_add(
                'operators',
                self.channel_name,
            )
        else:
            await self.channel_layer.group_add(
                user.group_name,
                self.channel_name,
            )
        await self.accept()

    async def disconnect(self, code):
        user = await get_user(self.scope['user_id'])
        await self.channel_layer.group_discard(
            user.group_name,
            self.channel_name
        )

    async def receive(self, text_data=None, bytes_data=None, **kwargs):
        OPERATOR_TYPES = [
            'operator.retrieve_clients',
            'operator.filter_clients',
            'operator.exclude_clients'
        ]
        try:
            content = json.loads(text_data)
            message_type = content.get('type')
            if message_type in OPERATOR_TYPES:
                await self.retrieve_clients(content)
        except json.JSONDecodeError:
            errmsg = {
                'error': 'JSON parse error'
            }
            await self.send(text_data=json.dumps(errmsg))
        # Called with either text_data or bytes_data for each frame
        # You can call:

    async def retrieve_clients(self, content):
        user = await get_user(self.scope['user_id'])
        if await user_belong(user, 'operators'):
            clients = {
                'type': content.get('type'),
                'data': await self.get_client_pending(content.get('filters', {}))
            }
            await self.send(text_data=json.dumps(clients))

    async def ot_creation(self, event):
        action = {
            'type': 'operator.fire_update'
        }
        await self.send(text_data=json.dumps(action))


class FallabSync(AsyncJsonWebsocketConsumer):

    async def connect(self):
        if self.scope['user_id'] is None:
            await self.close()
        user = await get_user(self.scope['user_id'])
        if user.status == 'disabled':
            await self.close()
        if await user_belong(user, 'operators'):
            # print(user.group_name)
            await self.channel_layer.group_add(
                user.group_name,
                self.channel_name,
            )
        else:
            await self.close()
        await self.accept()

    async def disconnect(self, code):
        user = await get_user(self.scope['user_id'])
        await self.channel_layer.group_discard(
            user.group_name,
            self.channel_name
        )

    async def send_result(self, content):
        # print(content)
        action = {
            'type': 'operator.result',
            'data': content.get('data')
        }
        await self.send(text_data=json.dumps(action))