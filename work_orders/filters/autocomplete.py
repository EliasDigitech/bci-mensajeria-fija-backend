from django_filters import rest_framework as filters
from ..models import AutoComplete

class AutocompleteFilter(filters.FilterSet):

    class Meta:
        model = AutoComplete
        fields = {
            'full_name': ['exact', 'icontains'],
        }
