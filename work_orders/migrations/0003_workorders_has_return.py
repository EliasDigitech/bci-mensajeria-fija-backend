# Generated by Django 3.0 on 2020-02-26 21:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('work_orders', '0002_auto_20200225_1259'),
    ]

    operations = [
        migrations.AddField(
            model_name='workorders',
            name='has_return',
            field=models.CharField(choices=[('no_return', 'Sin Retorno'), ('with_return', 'Con Retorno')], default='no_return', max_length=20),
        ),
    ]
