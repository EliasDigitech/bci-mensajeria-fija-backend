# django
from django.conf import settings

# rest_framework
from rest_framework import serializers

# models
from ..models import WorkOrders

from .WorkOrdersSerializer import WorkOrdersSerializer
import googlemaps
from googlemaps.exceptions import HTTPError

class FromContext(object):
    def __init__(self, value_fn):
        self.value_fn = value_fn

    def set_context(self, serializer_field):
        self.value = self.value_fn(serializer_field.context)

    def __call__(self):
        return self.value

class WorkOrdersExternalSerializer(WorkOrdersSerializer):
    sender_address = serializers.CharField(
        max_length=300,
        default=FromContext(lambda context: context.get('owner').branch.address)
        )
    sender_comune = serializers.CharField(
        max_length=100,
        default=FromContext(lambda context: context.get('owner').branch.comune)
        )
    sender_region = serializers.CharField(
        max_length=100,
        default=FromContext(lambda context: context.get('owner').branch.region)
        )
    sender_lat = serializers.DecimalField(
        max_digits=10, decimal_places=7,
        default=FromContext(lambda context: context.get('owner').branch.lat)
        )
    sender_lng = serializers.DecimalField(
        max_digits=10, decimal_places=7,
        default=FromContext(lambda context: context.get('owner').branch.lng)
        )

    gmaps = googlemaps.Client(key=settings.GMAPS_KEY)

    class Meta:
        model = WorkOrders
        fields = '__all__'
        extra_kwargs = {
            'service_name': {'required': False},
            'current_step': {'required': False},
            'product_name': {'required': False},
            'sender_address': {'required': False},
            'sender_lat': {'required': False},
            'sender_lng': {'required': False},
            'owner': {'required': False},
        }
        read_only_fields = ('barcode',)

    def validate_sender_address(self, value):
        if value == self.context.get('owner').branch.address:
            return value
        try:
            sender_geocode = self.gmaps.geocode(value, region="CL")
            if len(sender_geocode) >= 1:
                components = sender_geocode[0]['address_components']
                self.initial_data['sender_comune'] = next(
                    comp['long_name'] for comp in components
                    if comp['types'][0] == "administrative_area_level_3"
                    )
                self.initial_data['sender_region'] = next(
                    comp["long_name"] for comp in components
                    if comp["types"][0] == "administrative_area_level_1"
                    )
                sender_location = sender_geocode[0]["geometry"]["location"]
                self.initial_data['sender_lat'] = round(sender_location.get("lat", 0), 7)
                self.initial_data['sender_lng'] = round(sender_location.get("lng", 0), 7)

                return "{} {}".format(
                        next(
                        comp["long_name"] for comp in components
                        if comp["types"][0] == "route"
                        ),
                        next(
                        comp["long_name"] for comp in components
                        if comp["types"][0] == "street_number"
                        )
                    )
            else:
                raise serializers.ValidationError("Dirección inválida")
        except (HTTPError, StopIteration):
                raise serializers.ValidationError("Dirección inválida")
            
    def validate_receiver_address(self, value):
        try:
            receiver_geocode = self.gmaps.geocode(value, region="CL")
            if len(receiver_geocode) >= 1:
                components = receiver_geocode[0]['address_components']
                self.initial_data['receiver_comune'] = next(
                    comp['long_name'] for comp in components
                    if comp['types'][0] == "administrative_area_level_3"
                    )
                self.initial_data['receiver_region'] = next(
                    comp["long_name"] for comp in components
                    if comp["types"][0] == "administrative_area_level_1"
                    )
                receiver_location = receiver_geocode[0]["geometry"]["location"]
                self.initial_data['receiver_lat'] = round(receiver_location.get("lat", 0), 7)
                self.initial_data['receiver_lng'] = round(receiver_location.get("lng", 0), 7)

                return "{} {}".format(
                        next(
                        comp["long_name"] for comp in components
                        if comp["types"][0] == "route"
                        ),
                        next(
                        comp["long_name"] for comp in components
                        if comp["types"][0] == "street_number"
                        )
                    )
            else:
                raise serializers.ValidationError("Dirección inválida")
        except (HTTPError, StopIteration):
            raise serializers.ValidationError("Dirección inválida")
    