from rest_framework import serializers

class WorkOrdersSocketSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    commercial_business = serializers.CharField(max_length=200)
    works_orders = serializers.IntegerField()
