from .models import WorkOrdersSteps

def fetchChilden(current_step, depth=1):
    #ALLOWED_STATUSES = (2, 5)
    #if not current_step.status.code in ALLOWED_STATUSES:
    #    return WorkOrdersSteps.objects.filter(parent=current_step.pk)
    #current_level_steps = OTFlowSteps.objects.filter(parent=current_step_pk)
    search_parents = [current_step.pk,]
    current_level_parents = [current_step.pk,]
    depth -= 1
    for level in range(depth):
        current_level_steps = WorkOrdersSteps.objects.filter(parent__in=current_level_parents)
        current_level_parents = []
        for current_level in current_level_steps:
            search_parents.append(current_level.pk)
            current_level_parents.append(current_level.pk)
    return WorkOrdersSteps.objects.filter(parent__in=search_parents)