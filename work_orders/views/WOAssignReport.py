# django
from django.db import transaction
from django.conf import settings
from django.core.files.base import ContentFile
from django_filters.rest_framework import DjangoFilterBackend
from django.http import HttpResponse
from django.utils import timezone

# rest framework
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.exceptions import NotFound
from rest_framework.settings import api_settings

# etc
from io import BytesIO
import os
import datetime
import base64
import binascii
from ..permissions import IsOperatorUser
from work_orders.filters.workOrders import WOLogFilter, ManifestFilter

# serializer
from ..serializers.AssignManifestSerializer import AssignManifestSerializer, WOAssignLogListSerializer

# models
from work_orders.models import WorkOrders, WorkOrdersLog, AssignManifest
from management.models import User

# reportlab
from reportlab.lib import pagesizes
import reportlab.lib.units as units
from reportlab.lib.enums import TA_CENTER, TA_LEFT, TA_RIGHT
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import Image, Table, TableStyle, Paragraph, Spacer, BaseDocTemplate, PageBreak, NextPageTemplate, Frame, PageTemplate
from reportlab.platypus.flowables import TopPadder

class WOAssignReport(CreateAPIView, ListAPIView):
    serializer_class = AssignManifestSerializer
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, IsOperatorUser,)
    queryset = AssignManifest.objects.all().order_by('-created_at')
    filter_backends = (DjangoFilterBackend, )
    filterset_class = ManifestFilter
    
    @transaction.atomic
    def create(self, request, *args, **kwargs):
        try:
            transporter = User.objects.get(pk=request.data.get('transporter'))
        except (User.DoesNotExist, ValueError):
            raise NotFound

        assignments = WOLogFilter(data=request.data, queryset=WorkOrdersLog.objects.all()).qs
        if assignments.count() == 0:
            return Response(data={'transporter': 'No posee asignaciones'}, status=status.HTTP_400_BAD_REQUEST)
        manifest_wos = []
        orders = request.data.get('orders', list)
        for index, el in enumerate(assignments.values('pk')):
            try:
                manifest_wos.append({
                    'order': 0,
                    'wo_log': el['pk']
                })
            except IndexError:
                return Response(data={'orders': 'Cantidas de rutas calculadas difiere de OTs asignadas'}, status=status.HTTP_400_BAD_REQUEST)
        ser = self.serializer_class(data={
            'user' : self.request.user.pk,
            'manifest_wos': manifest_wos,
            **request.data
            })
        ser.is_valid(raise_exception=True)
        ser.save()
        instance = ser.instance

        buffer = BytesIO()
        document = BaseDocTemplate(
            buffer,
            pagesize=pagesizes.LETTER,
            leftMargin=0.8*units.cm,
            rightMargin=0.8*units.cm,
            topMargin=1.5*units.cm,
            bottomMargin=1.2*units.cm,
            title='Manifiesto de Carga',
            # showBoundary=1,
            # initialFontSize=3
        )
        portrait_frame = Frame(document.leftMargin, document.bottomMargin, document.width, document.height, id='portrait_frame ')
        landscape_frame = Frame(document.leftMargin, document.bottomMargin, document.height, document.width, id='landscape_frame ')

        document.addPageTemplates([
            PageTemplate(id='portrait', frames=portrait_frame),
            PageTemplate(id='landscape', frames=landscape_frame, pagesize=pagesizes.landscape(pagesizes.letter)),
            ])

        STYLES = getSampleStyleSheet()
        STYLESN = STYLES['Normal']
        TEXT_CENTER = ParagraphStyle(
            name='center',
            parent=STYLES['Normal'],
            alignment=TA_CENTER,
            # fontSize=7,
            # leading=8
            )
        TEXT_RIGHT = ParagraphStyle(
            name='right',
            parent=STYLES['Normal'],
            alignment=TA_RIGHT,
            )
        TEXT_LEFT = ParagraphStyle(
            name='left',
            parent=STYLES['Normal'],
            alignment=TA_LEFT,
            # leading=10
            )
        elements = []

        conexxion_logo_path = os.path.join(settings.STATIC_ROOT, 'images/logo.jpeg')
        conexxxion_logo = Image(conexxion_logo_path, 3.1*units.cm, 7.4*units.mm)
        header_table = Table([
            [
                conexxxion_logo,
                Paragraph(
                    '<h1>Manifiesto de Carga</h1>',
                    ParagraphStyle(
                        name='center_heading',
                        parent=STYLES.get('Heading2'),
                        alignment=TA_CENTER
                    )
                ),
                Paragraph(
                    # '',
                    str(instance.serie),
                    STYLES.get('Heading3')
                )
            ]
        ], colWidths=(4*units.cm, 12*units.cm, 3*units.cm))
        elements.append(header_table)
        assignments = instance.manifest_wos.all()
        now = datetime.datetime.now()
        total = assignments.count()
        total_drops = assignments.filter(wo_log__step__status__code=5).count()
        last_wo = assignments.first()
        transporter_table = Table([
            [
                Paragraph(
                    '<b>Móvil:</b>',
                    TEXT_LEFT
                ),
                Paragraph(
                    '{} {}, {} {}'.format(
                        transporter.first_name,
                        transporter.last_name,
                        last_wo.wo_log.transporter_helper.first_name if last_wo.wo_log.transporter_helper else '',
                        last_wo.wo_log.transporter_helper.last_name if last_wo.wo_log.transporter_helper else ''
                    ),
                    TEXT_LEFT
                ),
                Paragraph(
                    instance.created_at.astimezone(timezone.get_current_timezone()).strftime('%d-%m-%Y'),
                    STYLES.get('Heading3')
                )
            ],
            [
                Paragraph(
                    '<b>Total:</b>',
                    TEXT_LEFT
                ),
                Paragraph(
                    '<b>{}</b> ({} retiros, {} entregas)'.format(
                        str(total),
                        str(total - total_drops),
                        str(total_drops)
                        ),
                    TEXT_LEFT
                ),
            ],
            [
                Paragraph(
                    '<b>Patente:</b>',
                    TEXT_LEFT
                ),
                Paragraph(
                   last_wo.wo_log.transport.plate,
                TEXT_LEFT
                )
            ]
        ], colWidths=(2*units.cm, 14*units.cm, 3*units.cm))
        elements.append(transporter_table)
        elements.append(Spacer(2*units.mm, 2*units.mm))
        wos_tables = [
            [
                Paragraph('', TEXT_LEFT),
                Paragraph('<b>Cód.</b>', TEXT_LEFT),
                Paragraph('<b>Cliente</b>', TEXT_LEFT),
                Paragraph('<b>T</b>', TEXT_LEFT),
                Paragraph('<b>Dirección Entrega/Retiro</b>', TEXT_LEFT),
                Paragraph('<b>C</b>', TEXT_LEFT),
                Paragraph('<b>Proc.</b>', TEXT_LEFT)
            ]
        ]
        index = 1
        for el in assignments:
            op_type = ''
            if el.wo_log.work_order.current_step.to_sender:
                op_type = 'E'
                address = '{}, {}'.format(
                    el.wo_log.work_order.sender_address,
                    el.wo_log.work_order.sender_comune
                )
            else:
                if el.wo_log.work_order.current_step.status.code == 2:
                    op_type = 'R'
                    address = '{}, {}'.format(
                        el.wo_log.work_order.sender_address,
                        el.wo_log.work_order.sender_comune
                    )
                else:
                    op_type = 'E'
                    address = '{}, {}'.format(
                        el.wo_log.work_order.receiver_address, 
                        el.wo_log.work_order.receiver_comune
                    )

            wos_tables.append([
                Paragraph(str(index), TEXT_LEFT),
                Paragraph(el.wo_log.work_order.barcode, TEXT_LEFT),
                Paragraph(el.wo_log.work_order.owner.branch.company.commercial_business, TEXT_LEFT),
                Paragraph(op_type, TEXT_LEFT),
                Paragraph(address, TEXT_LEFT),
                Paragraph(str(el.wo_log.work_order.qty), TEXT_LEFT),
                Paragraph('', TEXT_LEFT)
            ])
            index += 1
        w_table = Table(
            wos_tables,
            colWidths=(1*units.cm, 2*units.cm, 3.2*units.cm, 0.7*units.cm, 6.8*units.cm, 1*units.cm, 4.4*units.cm)
        )
        w_table.setStyle(
            TableStyle([
                ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                # ('LEFTPADDING', (0, 0), (-1, -1), 1),
                # ('RIGHTPADDING', (0, 0), (-1, -1), 1),
                ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                # ('TOPPADDING', (0, 0), (-1, -1), 0),
                # ('BOTTOMPADDING', (0, 0), (-1, -1), 0)
            ])
        )
        elements.append(w_table)

        footer = [
            [
                Paragraph(
                    'Generado: <br/>'+ instance.created_at.astimezone(timezone.get_current_timezone()).strftime('%d-%m-%Y %H:%M:%S'),
                    # '',
                    TEXT_LEFT
                ),
                Paragraph(
                    '___________________________<br/>{} {}'.format(
                        transporter.first_name,
                        transporter.last_name
                    ),
                    TEXT_CENTER
                ),
            ]
        ]
        if last_wo.wo_log.transporter_helper:
            footer[0].append(
                Paragraph('___________________________<br/>{} {}'.format(
                    last_wo.wo_log.transporter_helper.first_name if last_wo.wo_log.transporter_helper else '',
                    last_wo.wo_log.transporter_helper.last_name if last_wo.wo_log.transporter_helper else ''
                ), TEXT_CENTER),
            )

        footer_table = Table(footer)
        footer_table.setStyle(
            TableStyle([
                # ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                # ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                # ('LEFTPADDING', (0, 0), (-1, -1), 1),
                # ('RIGHTPADDING', (0, 0), (-1, -1), 1),
                ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                # ('TOPPADDING', (0, 0), (-1, -1), 0),
                # ('BOTTOMPADDING', (0, 0), (-1, -1), 0)
            ])
        )
        elements.append(TopPadder(footer_table))
        # b64_map_picture = request.data.get('map_picture')
        # if 'data:' in b64_map_picture and ';base64,' in b64_map_picture:
        #     # Break out the header from the base64 content
        #     header, data = b64_map_picture.split(';base64,')

        #     # Try to decode the file. Return validation error if it fails.
        #     try:
        #         decoded_file = BytesIO(base64.b64decode(data))
        #         decoded_file.seek(0)
        #         map_picture = Image(decoded_file, width=24 * units.cm, height=17 * units.cm)
        #         elements.append(NextPageTemplate('landscape'))
        #         elements.append(PageBreak())
        #         elements.append(map_picture)
        #     except (TypeError, binascii.Error):
        #         pass
        
        document.build(elements)
        pdf = buffer.getvalue()
        buffer.close()

        instance.pdf.save("AssignReport.pdf", ContentFile(pdf))
        # ser.instance.save()

        response = HttpResponse(
            pdf,
            content_type='application/pdf'
        )
        response['Content-Disposition'] = 'inline; filename=AssignReport.pdf'
        response.write(pdf)
        return response

class WOAssignReportList(ListAPIView):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated,)
    serializer_class = WOAssignLogListSerializer
    queryset = WorkOrdersLog.objects.all()
    filter_backends = (DjangoFilterBackend, )
    filterset_class = WOLogFilter
