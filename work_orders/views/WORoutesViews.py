# rest framwrork
from rest_framework.generics import CreateAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import NotFound
from rest_framework.settings import api_settings

from ..serializers.WORoutesSerializer import TransporterRoutesSerializer
from ..permissions import IsOperatorUser
from ..models import TransporterRoutes

class WORoutesView(CreateAPIView, RetrieveAPIView):
    permission_classes = (*api_settings.DEFAULT_PERMISSION_CLASSES, IsAuthenticated, IsOperatorUser,)
    serializer_class = TransporterRoutesSerializer

    def get_serializer_context(self):
        cont = super().get_serializer_context()
        cont['user'] = self.request.user
        return cont

    def get_object(self):
        try:
            return TransporterRoutes.objects.get(
                transporter=self.request.GET.get("transporter"),
                created_at__date=self.request.GET.get("log_date")
            )
        except TransporterRoutes.DoesNotExist:
            raise NotFound